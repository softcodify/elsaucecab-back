const { Schema, model } = require('mongoose')

const encuestaCollection = 'Encuesta'

const schemaEncuesta = new Schema({
    alojamiento: {
        limpieza: { type: String, required: true },
        equipamientoCumpleExpectativas: { type: String, required: true },
        instalaciones: { type: String },
    },
    servicios: {
        calidadPersonal: { type: String, required: true },
        serviciosAdicionales: { type: String },
    },
    experienciaGeneral: {
        aspectosPositivos: { type: String },
        aspectosMejorables: { type: String },
    },
    probabilidadRecomendacion: { type: String, required: true }, 
    comentariosAdicionales: { type: String },
},
    {
        timestamps: true
    }
)

const Encuesta = model(encuestaCollection, schemaEncuesta)

module.exports = Encuesta