const Encuesta = require('../models/encuesta.model')

const controller = {
  getEncuesta: async (req, res) => {
    try {
      const encuestas = await Encuesta.find()
      
      return res.status(200).json({ 
        success: true,
        message: "Se obtuvieron las encuestas de forma exitosa",
        encuestas
      })
    } catch (error) {
      console.log('error: ', error)
      return res.status(500).json({
        success: false,
        message: 'Error al obtener las encuestas'
      })
    }
  },

  postEncuesta: async (req, res) => {
    try {
      const newEncuesta = await Encuesta.create(req.body)

      return res.status(201).json({
        success: true,
        message: "Encuesta creada",
        newEncuesta
      })
    } catch (error) {
      console.log('error: ', error)
      return res.status(500).json({
        success: false,
        message: 'Error al crear encuesta'
      })
    }
  },

  deleteEncuesta: async (req, res) => {
    res.json({ message: "deleteEncuesta" })
  }
}

module.exports = controller