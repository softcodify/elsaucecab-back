const encuestaRouter = require('./encuesta.router.js')

const router = (app) => {
  app.use('/api/survey', encuestaRouter)
}

module.exports = router