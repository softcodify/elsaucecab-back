const { Router } = require('express')
const router = Router()

const encuestaController = require('../controllers/encuesta.controller')

router.get('/', encuestaController.getEncuesta)
router.post('/', encuestaController.postEncuesta)
router.delete('/', encuestaController.deleteEncuesta)

module.exports = router
